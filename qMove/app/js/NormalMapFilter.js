/**
 * @author Mat Groves http://matgroves.com/ @Doormat23
 */


/**
 * 
 * The NormalMapFilter class uses the pixel values from the specified texture (called the displacement map) to perform a displacement of an object. 
 * You can use this filter to apply all manor of crazy warping effects
 * Currently the r property of the texture is used offset the x and the g propery of the texture is used to offset the y.
 * @class NormalMapFilter
 * @contructor
 * @param texture {Texture} The texture used for the displacemtent map * must be power of 2 texture at the moment
 */
 PIXI.NormalMapFilter = function(texture)
 {
    PIXI.AbstractFilter.call( this );

    this.passes = [this];
    //texture.baseTexture._powerOf2 = true;

    // set the uniforms
    //console.log()
    this.uniforms = {
        displacementMap: {type: 'sampler2D', value:texture},
        //offset:            {type: '2f', value:{x:0, y:0}},
        mapDimensions:   {type: '2f', value:{x:1, y:1}},
        lightDir: {type: '3fv', value:[0, 1, 0]},
        lightPos: {type: '3fv', value:[0, 1, 0]}
    };
    

    if(texture.baseTexture.hasLoaded) {
        console.log(texture.width, texture.height);
        this.uniforms.mapDimensions.value.x = texture.width;
        this.uniforms.mapDimensions.value.y = texture.height;
    } else {
        this.boundLoadedFunction = this.onTextureLoaded.bind(this);
        texture.baseTexture.on("loaded", this.boundLoadedFunction);
    }

    this.fragmentSrc = `
    precision mediump float;
    varying vec2 vTextureCoord;
    uniform sampler2D displacementMap;
    uniform sampler2D uSampler;
    uniform vec2 offset;
    vec2 mapDimensions = vec2(704, 384);
    uniform vec3 lightPos;
    uniform vec3 lightDir;

    const vec2 Resolution = vec2(1.0,1.0);      //resolution of screen
    const vec4 LightColor = vec4(1.0, 1.0, 1.0, 1.0);      //light RGBA -- alpha is intensity
    const vec4 AmbientColor = vec4(1.0, 1.0, 1.0, 0.9);    //ambient RGBA -- alpha is intensity 
    const vec3 Falloff = vec3(0.0, 0.1, 0.4);         //attenuation coefficients

    
    void main(void) {
        vec2 mapCords = vTextureCoord.xy * (vec2(320, 320) / mapDimensions);

        //RGBA of our diffuse color
        vec4 DiffuseColor = texture2D(uSampler, vTextureCoord);
        //RGB of our normal map
        vec3 NormalMap = texture2D(displacementMap, mapCords).rgb;

        //Determine distance (used for attenuation) BEFORE we normalize our lightDir
        float D = length(lightDir);

        //normalize our vectors
        vec3 N = normalize(NormalMap * 2.0 - 1.0);
        vec3 L = normalize(lightDir);

        //Pre-multiply light color with intensity
        //Then perform N dot L to determine our diffuse term
        vec3 Diffuse = (LightColor.rgb * LightColor.a) * max(dot(N, L), 0.0);

        //pre-multiply ambient color with intensity
        vec3 Ambient = AmbientColor.rgb * AmbientColor.a;

        //calculate attenuation
        float Attenuation = 1.0 / ( Falloff.x + (Falloff.y*D) + (Falloff.z*D*D) );
        Attenuation *= 0.5;
        Attenuation = min(Attenuation, 2.0);
        //the calculation which brings it all together
        vec3 Intensity = Ambient + Diffuse * Attenuation;
        vec3 FinalColor = DiffuseColor.rgb * Intensity;
        gl_FragColor = vec4(FinalColor, DiffuseColor.a);
        
        //gl_FragColor = vec4(mapDimensions.xy, 0.0, 1.0);
    }
    `;

}

PIXI.NormalMapFilter.prototype = Object.create( PIXI.AbstractFilter.prototype );
PIXI.NormalMapFilter.prototype.constructor = PIXI.NormalMapFilter;

PIXI.NormalMapFilter.prototype.onTextureLoaded = function() {

    this.uniforms.mapDimensions.value.x = this.uniforms.displacementMap.value.width;
    this.uniforms.mapDimensions.value.y = this.uniforms.displacementMap.value.height;

    this.uniforms.displacementMap.value.baseTexture.off("loaded", this.boundLoadedFunction)
}

/**
 * The texture used for the displacemtent map * must be power of 2 texture at the moment
 *
 * @property map
 * @type Texture
 */
 Object.defineProperty(PIXI.NormalMapFilter.prototype, 'map', {
    get: function() {
        return this.uniforms.displacementMap.value;
    },
    set: function(value) {
        this.uniforms.displacementMap.value = value;
    }
});

/**
 * The lightPos used.
 *
 * @property offset
 * @type Point
 */
 Object.defineProperty(PIXI.NormalMapFilter.prototype, 'lightPos', {
    get: function() {
        return this.uniforms.lightPos.value;
    },
    set: function(value) {
        this.uniforms.lightPos.value = value;
    }
});

/**
 * The offset used to move the displacement map.
 *
 * @property offset
 * @type Point
 */
 Object.defineProperty(PIXI.NormalMapFilter.prototype, 'offset', {
    get: function() {
        return this.uniforms.offset.value;
    },
    set: function(value) {
        this.uniforms.offset.value = value;
    }
});