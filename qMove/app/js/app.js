var settings = {
	width: 320,
	height: 320,
	toLoad: ["img/player.png", "img/tiles.png", "maps/test.json"]
}

var world = undefined;
var player = undefined;

var setup = () => {

	world = g.makeTiledWorld("maps/test.json", "img/tiles.png");

	//player
	player = g.sprite("img/player.png");

	player.x = 128;
	player.y = 128;
	player.tx = player.x;
	player.ty = player.y;

	player.questions = [];
	player.questionsText = [];

	/*
	let filter = new PIXI.filters.DropShadowFilter();
	filter.distance = 3;
	filter.blur = 0
	player.filters = [filter];
	*/

	//setup questions
	for(let i=0; i<4; i++) {
		let questionText = new PIXI.Text('', {font : '12px Helvetica', fill : 0x111111, align : 'center'});
		questionText.x = Math.cos(Math.PI / 2 * i ) * 32 + 16;
		questionText.y = Math.sin(Math.PI / 2 * i ) * 32 + 16;
		questionText.anchor.x = Math.cos(Math.PI / 2 * (-i+2) ) * 0.5 + 0.5;
		questionText.anchor.y = Math.sin(Math.PI / 2 * (-i+2) ) * 0.5 + 0.5;
		player.questionsText[i] = questionText;
		player.addChild(questionText);
	}

	player.changeQuestion = (side) => {
		if(side < 0 || side > 3) {
			throw "Wrong questin side "+side;
		}

		let q = matmat.getQuestion();
		player.questions[side] = q;

		player.questionsText[side].text = q.question;
	}

	player.move = (side) => {
		
		var tx = player.tx + Math.cos(Math.PI / 2 * side ) * world.tilewidth;
		var ty = player.ty + Math.sin(Math.PI / 2 * side ) * world.tileheight;

		player.tx = tx; player.ty = ty;
		g.slide(player, player.tx, player.ty, 10);

		player.changeQuestion(side);
	}

	player.changeQuestions = () => {
		for(let i=0; i<4; i++) {
			player.changeQuestion(i);
		}
	};

	player.changeQuestions();

	g.state = play;
};

var play = () => {

};

var pressedNumbersString = "";
document.addEventListener("keydown", (e) => {

	var pressedNumber = undefined;

	if(e.keyCode >= 48 && e.keyCode <= 57) {
		pressedNumber = e.keyCode - 48;
	}
	if(e.keyCode >= 96 && e.keyCode <= 105) {
		pressedNumber = e.keyCode - 96;
	}

	if(pressedNumber != undefined) {
		pressedNumbersString += pressedNumber;
		checkAnswer();
	}
});

var checkAnswer = () => {
	player.questions.forEach((q, i) => {
		if(pressedNumbersString.endsWith(q.answer)) {
			player.move(i);
			pressedNumbersString = "";
		}
	});
};

var g = hexi(settings.width, settings.height, setup, settings.toLoad);
g.scaleToWindow("black");
g.start();