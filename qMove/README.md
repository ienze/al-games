## qMove
Game prototype using questions from learning system to move.

## How to run
Install and run gulp
```
$ npm install --global gulp-cli
$ gulp
```

Start any web server in dist/ directory.