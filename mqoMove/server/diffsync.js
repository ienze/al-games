module.exports = function (server) {
	var io = require('socket.io')(server);

    // setting up diffsync's DataAdapter 
    var diffSync = require('diffsync');
    var dataAdapter = new diffSync.InMemoryDataAdapter();

    // setting up the diffsync server 
    var diffSyncServer = new diffSync.Server(dataAdapter, io);

    //Setup initial structure
    dataAdapter.storeData("__rooms__", [], function(err){
        if(err) {
            console.log(err);
        }
    });


    setInterval(function(){
    	dataAdapter.getData('__rooms__', function(err, data){
            if(err) {
                console.log(err);
            } else {
                console.log("rooms:");
                for(i in data) {
                    console.log(data[i].name+":");
                    dataAdapter.getData(data[i].id, function(err, rdata){
                        if(err) {
                            console.log(err);
                        } else {
                            console.log(rdata);
                        }
                    });
                }
            }
        });
    }, 6000);

};