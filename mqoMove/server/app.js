var fs = require("fs");
var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var app = express();

var accessLogStream = fs.createWriteStream(__dirname + '/access.log', {flags: 'a'})

// uncomment after placing your favicon in /public
app.use(favicon(__dirname + '/../dist/favicon.ico'));
app.use(logger('common', {stream: accessLogStream}));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, '/../dist')));

//because nginx is used.. DISABLE if it is not!
// app.set('trust proxy', true);

//redirect www. to non-www.
function wwwRedirect(req, res, next) {
	if (req.headers.host.slice(0, 4) === 'www.') {
		var newHost = req.headers.host.slice(4);
		return res.redirect(301, req.protocol + '://' + newHost + req.originalUrl);
	}
	next();
}
app.use(wwwRedirect);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
	var err = new Error('Not Found');
	err.status = 404;
	next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
	console.log('DEVELOPMENT MODE');

	app.use(function (err, req, res, next) {
		res.status(err.status || 500);
		res.send({
			siteTitle: 'Error',
			title: err.message,
			message: err.message,
			error: err
		});
	});

} else {
	// production error handler
	// no stacktraces leaked to user
	app.use(function (err, req, res, next) {
		res.status(err.status || 500);
		res.send({
			siteTitle: 'Error',
			title: err.message,
			message: err.message,
			error: {}
		});
	});
}

module.exports = app;