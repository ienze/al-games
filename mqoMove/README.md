## mqMove
Multiplayer game prototype using questions from learning system to move.

## How to run
Install and run gulp
```
$ npm install --global gulp-cli
$ gulp
```

Start server to handle multiplayer communication / webserver with game
```
$ npm start
```