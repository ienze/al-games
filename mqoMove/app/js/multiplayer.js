export default class Multiplayer {

	init(room) {
		var client = new diffsync.Client(window.io(), room);
	
		client.initialize();
		
		return client;
	}
}