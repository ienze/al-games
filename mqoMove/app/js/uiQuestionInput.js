export default class UiQuestionInput {

	constructor() {
		this.ui = document.querySelector("#ui");
		scaleToWindow(this.ui);
	}

	show(checkAnswerCallback) {
		this.ui.innerHTML = "<form>"
			+ "<input type=\"number\" name=\"answer\" />"
			+ "<input type=\"submit\" />"
		+ "</form>";
		
		var ui_form = ui.querySelector("form");
		var ui_form_input = ui_form.querySelector("input[name=\"answer\"]");
		ui_form.addEventListener("submit", (e) => {
			e.preventDefault();

			var clear = checkAnswerCallback(ui_form_input.value);
			if(clear) {
				ui_form_input.value = "";
				ui_form_input.className = "correct";
			} else {
				ui_form_input.className = "incorrect";
			}

			return false;
		});
	}

	hide() {
		this.ui.innerHTML = "";
	}
};
