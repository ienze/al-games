export default class StageLobby {

	constructor(g, multiplayer, stages, spriteSyncUtils) {
		this.g = g;
		this.multiplayer = multiplayer;
		this.stages = stages;
		this.spriteSyncUtils = spriteSyncUtils;

		this.rooms = undefined;
		this.createRoom = undefined;
	}

	load() {

		var self = this;

		self.rooms = new PIXI.Container();
		self.rooms.x = 16;
		self.rooms.y = 64;

		var drc = this.multiplayer.init("__rooms__");
		var dr = undefined;

		var synced = function() {

			if(self.rooms == undefined) {
				return; //Synced when already left this state, ignore
			}
			
			self.spriteSyncUtils.recreate(self.rooms, dr, (droom, i) => {
				var room = new PIXI.Container();
				room.y = i * 50;
				
				let roomName = new PIXI.Text(droom.name, {font : '12px Helvetica', fill : 0x111111});
				
				room.addChild(roomName);

				room.interactive = true;
				room.on('click', (function(id) {
					return function(e) {
						self.stages.getStage("play").room = id;
						self.stages.changeStage("play");
					}
				})(droom.id));

				return room;
			});
		}

		drc.on('connected', function() {
			dr = drc.getData();
			synced();
		});

		drc.on('synced', synced);

		/*
		 * Gui buttons
		 */

		this.createRoom = new PIXI.Text("Create room", {font : '12px Helvetica', fill : 0x111111});
		this.createRoom.x = 16;
		this.createRoom.y = 16;
		this.createRoom.interactive = true;
		this.createRoom.on('click', function(event) {

			if(!Array.isArray(dr)) {
				return; //Not connected yet.
			}

			let randomId = (Math.random().toString(36)+'00000000000000000').slice(2, 8);
			
			dr.push({'id': randomId, 'name': randomId});
			drc.sync();

			self.stages.getStage("play").room = randomId;
			self.stages.changeStage("play");
		});


		this.g.stage.addChild(this.createRoom);
		this.g.stage.addChild(this.rooms);

		this.g.state = this.play;
	}

	unload() {
		this.g.stage.removeChild(this.rooms);
		this.g.stage.removeChild(this.createRoom);
		this.rooms = undefined;
		this.createRoom = undefined;
	}

	play() {

	}
}