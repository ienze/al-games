export default class StagePlay {

	constructor(g, multiplayer, stages, matmat, spriteSyncUtils, uiQuestionInput, settings) {
		this.g = g;
		this.multiplayer = multiplayer;
		this.stages = stages;
		this.matmat = matmat;
		this.spriteSyncUtils = spriteSyncUtils;
		this.uiQuestionInput = uiQuestionInput;
		this.settings = settings;
		
		this.worldOffsets = [2, 1, 1, 1]; //top right bottom left

		this.tileWidth = 48; 
		this.tileHeight = 48;
		this.worldWidth = (this.settings.width / this.tileWidth) - (this.worldOffsets[1] + this.worldOffsets[3]);
		this.worldHeight = (this.settings.height / this.tileHeight) - (this.worldOffsets[0] + this.worldOffsets[2]);
		this.worldSize = this.worldWidth * this.worldHeight;

		this.room = undefined;

		this.tileTextures = undefined;
		this.background = undefined;
		this.world = undefined;
		this.player = undefined;
		this.players = undefined;
		this.gui = undefined;

		this.mes = {};
	}

	load() {
		var self = this;

		if(!this.room) {
			throw "Room id not set!";
		}

		self.tileTextures = self.g.filmstrip("img/tiles.png", self.tileWidth, self.tileHeight);

		self.background = self.g.rectangle(self.settings.width, self.settings.height, "#000000", "#000000", 0, 0, 0);
		self.world = self.g.grid(
			self.worldWidth,
			self.worldHeight,
			self.tileWidth, self.tileHeight, false, 0, 0,
			() => {
				let tile = self.g.sprite(self.tileTextures[0]);
				return tile;
			});
		self.world.x = self.tileWidth * self.worldOffsets[3];
		self.world.y = self.tileHeight * self.worldOffsets[0];

		self.players = self.g.group();
		self.players.x = self.tileWidth * self.worldOffsets[3];
		self.players.y = self.tileHeight * self.worldOffsets[0];
		
		self.gui = self.g.group();
		//
		// Score gui
		//
		let guiScoreRect = self.g.rectangle(80, 37, "#F1F1F1", "#000000", 1, self.settings.width / 2 - 60, -1);
		self.gui.addChild(guiScoreRect);

		let guiScoreText = new PIXI.Text('0%', {font : '21px Helvetica', fill : 0x000000});
		guiScoreText.x = self.settings.width / 2;
		guiScoreText.y = 8;
		guiScoreText.anchor.x = 0.5;
		self.gui.addChild(guiScoreText);

		//
		// Controls gui
		//
		let guiBack = self.g.rectangle(100, 37, "#F1F1F1", "#000000", 1, -1, -1);
		guiBack.interactive = true;
		guiBack.on('click', () => {
			self.stages.changeStage("lobby");
		});
		self.gui.addChild(guiBack);

		let guiBackText = new PIXI.Text('Back', {font : '16px Helvetica', fill : 0x000000});
		guiBackText.x = 8;
		guiBackText.y = 8;
		guiBack.addChild(guiBackText);

		let guiScoreboard = self.g.rectangle(100, 37, "#F1F1F1", "#000000", 1, self.settings.width - 100 + 1, -1);
		guiScoreboard.interactive = true;
		guiScoreboard.on('click', () => {
			self.stages.getStage("scoreboard").room = self.room;
			self.stages.getStage("scoreboard").backStage = "play";
			self.stages.changeStage("scoreboard");
		});
		self.gui.addChild(guiScoreboard);

		let guiScoreboardText = new PIXI.Text('Scoreboard', {font : '16px Helvetica', fill : 0x000000});
		guiScoreboardText.x = 100 - 8;
		guiScoreboardText.y = 8;
		guiScoreboardText.anchor.x = 1.0;
		guiScoreboard.addChild(guiScoreboardText);

		self.g.stage.addChild(self.gui);

		//
		// Players and world
		//

		var drc = self.multiplayer.init(this.room);
		var dr = undefined;

		var initialiseGame = () => {
			if(!dr) {
				dr = {};
			}
			if(!dr.players) {
				dr.players = [];
				drc.sync();
			}
			if(!dr.world) {
				dr.world = [];
				for(let i=0; i<self.worldSize; i++) {
					dr.world.push(0);
				}
			}
		};

		var synced = () => {

			console.log("Synced");
			console.log(dr);

			if(self.players == undefined || !dr) {
				return; //Synced when already left this state, ignore
			}

			initialiseGame();

			//players
			self.spriteSyncUtils.sync(self.players, dr.players, (dplayer) => {
				let p = self.g.sprite("img/player.png");
				p.dplayer = dplayer;

				let playerTile = self.g.sprite(self.tileTextures[p.dplayer.tile]);
				playerTile.width = 16;
				playerTile.height = 16;
				playerTile.x = 16;
				playerTile.y = 10;
				p.addChild(playerTile);

				p.x = self.tileWidth * dplayer.x;
				p.y = self.tileHeight * dplayer.y;

				return p;
			}, (p) => {
				var tx = self.tileWidth * p.dplayer.x;
				var ty = self.tileHeight * p.dplayer.y;

				if(tx != p.x || ty != p.y) {
					self.g.slide(p, tx, ty, 10);
				}
			}, (p) => {
				return p.dplayer.id;
			});

			for(let i = 0; i<self.worldSize; i++) {
				self.world.children[i].texture = self.tileTextures[dr.world[i]];
			}

			if(self.player) {
				guiScoreText.text = Math.round(dr.world.reduce((acc, t) => {return acc + (t == self.player.dplayer.tile ? 1 : 0)}, 0) / self.worldSize * 100) + "%";
			}

			//make sure your player is on top
			self.players.children.sort(function(a,b) {
				return a == self.player;
			});
		}

		var addMe = () => {

			if(self.players == undefined) {
				return; //Synced when already left this state, ignore
			}

			let p = null;

			if(self.mes[self.room]) {
				//already is in game
				for(let i = 0; i<self.players.children.length; i++) {
					if(self.players.children[i].dplayer.id == self.mes[self.room]) {
						p = self.players.children[i];
					}
				}
			}

			if(!p) {
				initialiseGame();
				
				p = self.g.sprite("img/player.png");

				var freeTiles = [1, 2, 3, 4, 5, 6, 7];
				dr.players.forEach((dplayer) => {
					freeTiles.splice(freeTiles.indexOf(dplayer.tile), 1);
				});

				var randomId = (Math.random().toString(36)+'00000000000000000').slice(2, 8);
				p.dplayer = {
					id: randomId,
					name: randomId,
					x: 1,
					y: 1,
					tile: freeTiles[0]
				};

				let playerTile = self.g.sprite(self.tileTextures[p.dplayer.tile]);
				playerTile.width = 16;
				playerTile.height = 16;
				playerTile.x = 16;
				playerTile.y = 10;
				p.addChild(playerTile);

				self.mes[self.room] = randomId;

				self.players.addChild(p);

				dr.players.push(p.dplayer);
				drc.sync();
			}

			p.x = self.tileWidth * p.dplayer.x;
			p.y = self.tileHeight * p.dplayer.y;
			
			self.player = p;
			
			p.questions = [];
			p.questionsText = [];

			let questionTextFilter = new PIXI.filters.DropShadowFilter();
			questionTextFilter.color = 0x000000;
			questionTextFilter.alpha = 10;
			questionTextFilter.distance = 0;
			questionTextFilter.blur = 4;

			//setup questions
			for(let i=0; i<4; i++) {
				let questionText = new PIXI.Text('', {font : '12px Helvetica', fill : 0xffffff, align : 'center'});
				questionText.x = Math.cos(Math.PI / 2 * i ) * self.tileWidth + (self.tileWidth / 2);
				questionText.y = Math.sin(Math.PI / 2 * i ) * self.tileHeight + (self.tileHeight / 2);
				questionText.anchor.x = 0.5;
				questionText.anchor.y = 0.5;
				questionText.filters = [questionTextFilter];
				p.questionsText[i] = questionText;
				p.addChild(questionText);
			}

			p.changeQuestion = (side) => {
				if(side < 0 || side > 3) {
					throw "Wrong question side "+side;
				}

				let currentAnswers = p.questions
				.filter((q, i) => {return i == side})
				.map((q) => {return q.answer;});

				let q = self.matmat.getQuestion(currentAnswers);
				p.questions[side] = q;

				p.questionsText[side].text = q.question;
			}

			p.move = (side) => {
				
				var tx = Math.round((p.x / self.tileWidth) + Math.cos(Math.PI / 2 * side ));
				var ty = Math.round((p.y / self.tileHeight) + Math.sin(Math.PI / 2 * side ));

				if(tx < 0 || ty < 0 || tx >= self.worldWidth || ty >= self.worldHeight) {
					return;
				}

				p.dplayer.x = tx;
				p.dplayer.y = ty;

				dr.world[ty * self.worldWidth + tx] = p.dplayer.tile;

				synced();
				drc.sync();

				p.changeQuestion(side);
			}

			p.changeQuestions = () => {
				for(let i=0; i<4; i++) {
					p.changeQuestion(i);
				}
			}

			this.uiQuestionInput.show((answer) => {
				for(var i=0; i<p.questions.length; i++) {
					var q = p.questions[i];
					if(answer === q.answer) {
						p.move(i);
						return true;
					}
				}
				return false;
			});

			p.changeQuestions();
		}

		drc.on('connected', () => {
			dr = drc.getData();
			console.log("Connected");
			console.log(dr);
			// synced();
			addMe();
		});

		drc.on('synced', synced);

		self.g.state = this.play.bind(this);
	}

	play() {
		
	}

	unload() {
		this.uiQuestionInput.hide();

		this.g.stage.removeChild(this.background);
		this.g.stage.removeChild(this.world);
		this.g.stage.removeChild(this.players);
		this.g.stage.removeChild(this.gui);
		
		this.tileTextures = undefined;
		this.background = undefined;
		this.world = undefined;
		this.player = undefined;
		this.players = undefined;
		this.gui = undefined;
	}
}