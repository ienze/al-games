export default class MatMatData {

	constructor() {
		this.qid = 0;
		this.questions = [{"question":"100 - 75","answer":"25"},{"question":"4 \u00d7 8","answer":"32"},{"question":"7 + 8","answer":"15"},{"question":"9 + 4","answer":"13"},{"question":"8 + 6","answer":"14"},{"question":"13 + 4","answer":"17"},{"question":"7 + 5","answer":"12"},{"question":"14 + 4","answer":"18"},{"question":"6 + 12","answer":"18"},{"question":"10 \u00d7 14","answer":"140"},{"question":"9 \u00d7 7","answer":"63"},{"question":"3 + 8","answer":"11"},{"question":"15 + 5","answer":"20"},{"question":"28 \u00f7 4","answer":"7"},{"question":"10 + 6","answer":"16"},{"question":"14 + 1","answer":"15"},{"question":"63 \u00f7 7","answer":"9"},{"question":"1 + 11","answer":"12"},{"question":"2 \u00d7 8","answer":"16"},{"question":"8 + 1","answer":"9"},{"question":"2 + 7","answer":"9"},{"question":"7 - 4","answer":"3"},{"question":"2 + 8","answer":"10"},{"question":"4 + 6","answer":"10"},{"question":"5 \u00d7 4","answer":"20"},{"question":"8 - 2","answer":"6"},{"question":"4 \u00d7 14","answer":"56"},{"question":"4 + 8","answer":"12"},{"question":"12 \u00f7 3","answer":"4"},{"question":"8 + 7","answer":"15"},{"question":"3 \u00d7 3","answer":"9"},{"question":"7 \u00d7 10","answer":"70"},{"question":"6 \u00f7 3","answer":"2"},{"question":"4 + 14","answer":"18"},{"question":"6 + 3","answer":"9"},{"question":"6 + 6","answer":"12"},{"question":"4 + 1","answer":"5"},{"question":"9 - 1","answer":"8"},{"question":"11 + 1","answer":"12"},{"question":"13  ","answer":"13"},{"question":"9 - 2","answer":"7"},{"question":"36 \u00f7 6","answer":"6"},{"question":"45 \u00f7 5","answer":"9"},{"question":"8 \u00d7 17","answer":"136"},{"question":"15 \u00f7 3","answer":"5"},{"question":"7 - 1","answer":"6"},{"question":"8 - 1","answer":"7"},{"question":"17 \u00d7 8","answer":"136"},{"question":"18 - 13","answer":"5"},{"question":"50 - 15","answer":"35"},{"question":"55 - 35","answer":"20"},{"question":"25 - 10","answer":"15"},{"question":"17 - 13","answer":"4"},{"question":"40 + 47","answer":"87"},{"question":"30 - 25","answer":"5"},{"question":"31 + 37","answer":"68"},{"question":"31 + 64","answer":"95"},{"question":"18 - 5","answer":"13"},{"question":"19 - 16","answer":"3"},{"question":"36 + 43","answer":"79"},{"question":"14 - 8","answer":"6"},{"question":"34 + 31","answer":"65"},{"question":"12 - 4","answer":"8"},{"question":"7 + 32","answer":"39"},{"question":"45 - 10","answer":"35"},{"question":"50 - 10","answer":"40"},{"question":"17 - 14","answer":"3"},{"question":"15 - 11","answer":"4"},{"question":"20 - 9","answer":"11"},{"question":"17 - 9","answer":"8"},{"question":"19 - 7","answer":"12"},{"question":"25 - 20","answer":"5"},{"question":"18 - 12","answer":"6"},{"question":"11 - 6","answer":"5"},{"question":"46 + 9","answer":"55"},{"question":"20 - 2","answer":"18"},{"question":"16 - 2","answer":"14"},{"question":"15 - 1","answer":"14"},{"question":"15 - 4","answer":"11"},{"question":"15 - 13","answer":"2"},{"question":"11 - 4","answer":"7"},{"question":"13 - 3","answer":"10"},{"question":"75 - 25","answer":"50"},{"question":"13 - 9","answer":"4"},{"question":"60 - 30","answer":"30"},{"question":"11 - 6","answer":"5"},{"question":"75 - 10","answer":"65"},{"question":"18 - 15","answer":"3"},{"question":"17 - 17","answer":"0"},{"question":"12 - 1","answer":"11"},{"question":"70 - 70","answer":"0"},{"question":"19 - 18","answer":"1"},{"question":"37 + 48","answer":"85"}];
	}

	getQuestion(currentAnswers = []) {

		var shuffle = (a) => {
			for (let i = a.length; i; i--) {
				let j = Math.floor(Math.random() * i);
				[a[i - 1], a[j]] = [a[j], a[i - 1]];
			}
		}

		let q = null;
		do {
			if(this.qid >= this.questions.length) {
				this.qid = 0;
			}

			if(this.qid == 0) {
				shuffle(this.questions);
			}

			q = this.questions[this.qid++];
		} while (currentAnswers.includes(q.answer));
		return q;
	}
};
