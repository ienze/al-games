export default class StageScoreboard {

	constructor(g, multiplayer, stages, spriteSyncUtils) {
		this.g = g;
		this.multiplayer = multiplayer;
		this.stages = stages;
		this.spriteSyncUtils = spriteSyncUtils;

		this.room = undefined;
		this.backStage = "lobby";

		this.worldSize = 8 * 8;

		this.players = undefined;
		this.roomName = undefined;
		this.gui = undefined;
	}

	load() {
		var self = this;

		self.players = self.g.group();
		self.players.x = 16;
		self.players.y = 64;

		if(!this.room) {
			throw "Room id not set!";
		}

		var drc = self.multiplayer.init(this.room);
		var dr = undefined;
		
		var synced = function() {

			if(self.players == undefined) {
				return; //Synced when already left this state, ignore
			}
			
			console.log("Synced "+self.room);
			console.log(dr);

			if(dr.name) { //room name
				self.roomName.text = dr.name;
			}

			dr.players.forEach((p) => {
				p.score = Math.round(dr.world.reduce((acc, t) => {return acc + (t == p.tile ? 1 : 0)}, 0) / self.worldSize * 100)
			});

			dr.players.sort((a, b) => {
				return b.score - a.score;
			});

			console.log(self.players);

			self.spriteSyncUtils.recreate(self.players, dr.players, (player, i) => {
				let p = new PIXI.Container();
				p.y = i * 50;
				
				let pName = new PIXI.Text(player.name, {font : '12px Helvetica', fill : 0x111111});
				p.addChild(pName);

				let pScore = new PIXI.Text(player.score+"%", {font : '12px Helvetica', fill : 0x111111});
				pScore.anchor.x = 1;
				pScore.x = 300;
				p.addChild(pScore);

				return p;
			});

		};

		drc.on('connected', function() {
			dr = drc.getData();
			synced();
		});

		drc.on('synced', synced);

		this.roomName = new PIXI.Text("", {font : '12px Helvetica', fill : 0x111111});
		this.roomName.x = 16;
		this.roomName.y = 16;

		//
		// Controls gui
		//
		self.gui = self.g.group();

		if(self.backStage) {
			let guiBack = self.g.rectangle(100, 37, "#F1F1F1", "#000000", 1, -1, -1);
			guiBack.interactive = true;
			guiBack.on('click', () => {
				self.stages.changeStage(self.backStage);
			});
			self.gui.addChild(guiBack);

			let guiBackText = new PIXI.Text('Back', {font : '16px Helvetica', fill : 0x000000});
			guiBackText.x = 8;
			guiBackText.y = 8;
			guiBack.addChild(guiBackText);
		}

		this.g.state = this.play;
	}

	unload() {
		this.g.stage.removeChild(this.players);
		this.g.stage.removeChild(this.roomName);
		this.g.stage.removeChild(this.gui);

		this.players = undefined;
		this.roomName = undefined;
		this.gui = undefined;
	}

	play() {

	}
}