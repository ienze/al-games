import Stages from './stages';
import Multiplayer from './multiplayer';
import MatMatData from './MatMatData';
import StageLobby from './stageLobby';
import StagePlay from './stagePlay';
import StageScoreboard from './stageScoreboard';
import SpriteSyncUtils from './spriteSyncUtils';
import UiQuestionInput from './uiQuestionInput';

var settings = {
	width: 480,
	height: 528,
	fullpage: false,
	toLoad: ["img/player.png", "img/tiles.png"]
}

if(settings.fullpage) {
	settings.width = window.innerWidth;
	settings.height = window.innerHeight;
}

var g = hexi(settings.width, settings.height, function(){
	if(window.location.hash.startsWith("#play-")) {
		stages.getStage("play").room = window.location.hash.substr(6);
		stages.changeStage("play");
	} else if(window.location.hash.startsWith("#scoreboard-")) {
		stages.getStage("scoreboard").room = window.location.hash.substr(12);
		stages.changeStage("scoreboard");
	} else {
		stages.changeStage("lobby");
	}
}, settings.toLoad);

var stages = new Stages();
var multiplayer = new Multiplayer();
var matmat = new MatMatData();
var spriteSyncUtils = new SpriteSyncUtils();
var uiQuestionInput = new UiQuestionInput();
stages.addStage("lobby", new StageLobby(g, multiplayer, stages, spriteSyncUtils));
stages.addStage("play", new StagePlay(g, multiplayer, stages, matmat, spriteSyncUtils, uiQuestionInput, settings));
stages.addStage("scoreboard", new StageScoreboard(g, multiplayer, stages, spriteSyncUtils));

if(!settings.fullpage) {
	g.scaleToWindow("black");
}
g.start();

// window.app = { //Debugging purposes, remove after
// 	g,
// 	stages,
// 	multiplayer,
// 	matmat
// };