var settings = {
	width: 900,
	height: 900,
	toLoad: ["img/image.jpg"],
	size: 3,
	randomness: 12
}

var tiles = undefined;
var solution = [];

var setup = () => {

	//tiles
	tiles = g.group();

	tiles.switchTiles = (x1, y1, x2, y2, slide = false) => {

		var t1 = tiles.map[x1][y1];
		tiles.map[x1][y1] = null;
		var t2 = tiles.map[x2][y2];
		tiles.map[x2][y2] = null;

		tiles.map[x1][y1] = t2;
		tiles.map[x2][y2] = t1;
		tiles.map[x1][y1].moveTo(x1, y1, slide);
		tiles.map[x2][y2].moveTo(x2, y2, slide);
	}

	tiles.setupTiles = (size, randIter) => {

		tiles.children = [];
		tiles.map = [];
		tiles.size = size;

		var tileWidth = settings.width / size;
		var tileHeight = settings.height / size;

		for(let x=0; x<size; x++) {
			tiles.map.push([]);

			for(let y=0; y<size; y++) {
				
				var tile = g.spriteUtilities.group();
				tile.question = null;

				tile.x = tileWidth * x;
				tile.y = tileHeight * y;

				let background = g.spriteUtilities.sprite(g.frame("img/image.jpg", tileWidth * x, tileHeight * y, tileWidth, tileHeight));
				tile.addChild(background);
				tile.backgroundSprite = background;

				tile.moveTo = (function(tile) {
					return function(x, y, slide = false) {

						if(slide) {
							g.slide(tile, tileWidth * x, tileHeight * y, 10);
						} else {
							tile.x = tileWidth * x;
							tile.y = tileHeight * y;
						}
					}
				}(tile));

				tile.move = null;

				tiles.addChild(tile);
				tiles.map[x][y] = tile;
			}
		}

		var tileWidth = settings.width / tiles.size;
		var tileHeight = settings.height / tiles.size;

		var emptyX = size - 1;
		var emptyY = size - 1;

		// EMPTY
		tiles.emptyTile = tiles.map[emptyX][emptyY];
		tiles.emptyTile.backgroundSprite.visible = false;

		var questionText = new PIXI.Text('', {font : '40px Helvetica', fill : 0xffffff, align : 'center'});
		questionText.x = tileWidth / 2;
		questionText.y = tileHeight / 2;
		questionText.anchor.set(0.5, 0.5);
		
		let textBgFilter = new PIXI.filters.DropShadowFilter();
		textBgFilter.color = 0x000000;
		textBgFilter.alpha = 10;
		textBgFilter.distance = 0;
		textBgFilter.blur = 4;
		questionText.filters = [textBgFilter];

		questionText.updateQuestion = () => {
			questionText.text = solution[solution.length - 1].question.question;
		}
		
		tiles.emptyTile.questionText = questionText;
		tiles.emptyTile.addChild(questionText);


		// RANDOMIZE

		var randomized = [];
		for(let x=0; x<size; x++) {
			randomized.push([]);
			for(let y=0; y<size; y++) {
				randomized[x][y] = false;
			}
		}

		for (var i=0; i<randIter; i++) {
			var x, y;

			let unrandomizedSides = [];
			for(let r = 0; r < 1; r += 0.25) {
				let xr = emptyX + Math.round(Math.cos(r*Math.PI*2));
				let yr = emptyY + Math.round(Math.sin(r*Math.PI*2));

				if(xr < 0 || yr < 0 || xr >= size || yr >= size) {
					continue;
				}

				if(randomized[xr][yr]) {
					continue;
				}

				unrandomizedSides.push(r);
			}

			if(unrandomizedSides.lenght > 0) {
				let r = unrandomizedSides[Math.round(Math.random() * (unrandomizedSides.length - 1))];
				x = emptyX + Math.round(Math.cos(r*Math.PI*2));
				y = emptyY + Math.round(Math.sin(r*Math.PI*2));
			} else {
				do {
					let r = Math.floor(Math.random() * 4) / 4;
					x = emptyX + Math.round(Math.cos(r*Math.PI*2));
					y = emptyY + Math.round(Math.sin(r*Math.PI*2));
				} while(x < 0 || y < 0 || x >= size || y >= size);
			}


			randomized[x][y] = true;

			tiles.switchTiles(emptyX, emptyY, x, y);

			(function(movedTile, fromX, fromY, toX, toY) {
				solution.push({
					"question": matmat.getQuestion(),
					"callback": function() {
						tiles.switchTiles(fromX, fromY, toX, toY, true);
					}
				});
			}(tiles.map[emptyX][emptyY], emptyX, emptyY, x, y));
			
			emptyX = x; emptyY = y;
		}

		questionText.updateQuestion();
	};
	
	tiles.setupTiles(settings.size, settings.randomness);

	g.state = play;
};

var play = () => {

};

var pressedNumbersString = "";
document.addEventListener("keydown", (e) => {

	var pressedNumber = undefined;

	if(e.keyCode >= 48 && e.keyCode <= 57) {
		pressedNumber = e.keyCode - 48;
	}
	if(e.keyCode >= 96 && e.keyCode <= 105) {
		pressedNumber = e.keyCode - 96;
	}

	if(pressedNumber != undefined) {
		pressedNumbersString += pressedNumber;
		checkAnswer();
	}
});

var checkAnswer = () => {
	if(solution.length > 0) {
		if(pressedNumbersString.endsWith(solution[solution.length - 1].question.answer)) {
			solution[solution.length - 1].callback();
			solution.pop();

			if(solution.length == 0) {
				tiles.emptyTile.questionText.visible = false;
				tiles.emptyTile.backgroundSprite.visible = true;
			} else {
				tiles.emptyTile.questionText.updateQuestion();
			}
		}
	}
};

var g = hexi(settings.width, settings.height, setup, settings.toLoad);
g.scaleToWindow("black");
g.start();