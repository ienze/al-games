window.game.app = function() {

    var settings = {
        width: 1800,
        height: 900,
        showTime: true,
        showDistance: true,
        toLoad: ["img/stand.png", "img/run.png", "img/dust.png"]
    }

    var ui = document.querySelector("#ui");

    var dustFrames;

    var targetDistance = 300;
    var runSpeed = 0;
    var distance = 0;
    var fromTime = new Date();

    var questionText;
    var distanceText;
    var timeText;
    var runSprite;
    var standSprite;
    var path_bg, path;

    var setup = () => {

        dustFrames = g.filmstrip("img/dust.png", 50, 50);

        g.rectangle(settings.width, settings.height/2, "#A54E68", false, false, 0, settings.height/2);

        let runTextures = g.filmstrip("img/run.png", 50, 50);
        runSprite = g.sprite(runTextures);
        
        let standTextures = g.filmstrip("img/stand.png", 50, 50);
        standSprite = g.sprite(standTextures);

        runSprite.anchor.set(0.5, 0.5); standSprite.anchor.set(0.5, 0.5);
        runSprite.x = standSprite.x = settings.width / 2;
        runSprite.y = settings.height / 2 - 130;
        standSprite.y = settings.height / 2 - 170;
        runSprite.width = runSprite.height = 400;
        standSprite.width = standSprite.height = 400;

        questionText = g.text('???', '82px Helvetica', "white");
        questionText.anchor.set(0.5, 0.5);
        questionText.x = settings.width / 2;
        questionText.y = settings.height / 2 + 120;
        
        distanceText = g.text('0 m', '21px Helvetica', "#A54E68");
        distanceText.x = 20;
        distanceText.y = 20;
        distanceText.anchor.set(0.0, 0.0);
        distanceText.visible = settings.showDistance;

        timeText = g.text('00:00', '21px Helvetica', "#A54E68");
        timeText.x = settings.width - 20;
        timeText.y = 20;
        timeText.anchor.set(1.0, 0.0);
        timeText.visible = settings.showTime;
        
        path_bg = g.rectangle(settings.width - 400, 10, "#cccccc", false, false, 200, 20);
        path = g.rectangle(settings.width - 400, 10, "#A54E68", false, false, 200, 20);

        loadQuestion();
        updateSpriteState();

        g.state = play;
    };

    var play = () => {

        if(distance > targetDistance) {

            runSprite.x += 100 * runSpeed;

            return;
        }

        distance += runSpeed;
        distanceText.text = Math.round(distance) + " m / "+targetDistance + " m";

        path.width = distance / targetDistance * (settings.width - 400);
        console.log(path.width);

        runSpeed *= 0.99;

        updateSpriteState();

        if(Math.random() * runSpeed > 0.5) {
            g.createParticles(
                runSprite.x, runSprite.y + 130, //x and y position
                () => g.sprite("img/dust.png"),   //Particle sprite
                g.stage,                       //The container to add the particles to  
                Math.round(3 * Math.random() * runSpeed), //Number of particles
                0,                             //Gravity
                false,                         //Random spacing
                Math.PI, Math.PI + (0.6*runSpeed),                       //Min/max angle
                50 * runSpeed, 100 * runSpeed,                       //Min/max size
                15, 22                         //Min/max speed
                );
        }

        var timeDiff = new Date((new Date()) - fromTime);
        timeText.text = timeDiff.getMinutes() + ":" + timeDiff.getSeconds();
    };

    var animationLoop;
    var animationLoopFrame = 0;
    var updateSpriteState = () => {

        if(runSpeed > 0.02) {
            runSprite.visible = true;
            standSprite.visible = false;

            if(!animationLoop) {
                animationLoop = setTimeout(function(){
                    runSprite.gotoAndStop(animationLoopFrame++);
                    animationLoop = null;
                }, 10+(100*(1-runSpeed)));
            }

        } else {
            runSprite.visible = false;
            standSprite.visible = true;
        }
    }

    // document.addEventListener("keydown", (e) => {
        
    // });

    var ui_form = ui.querySelector("form");
    var ui_form_input = ui_form.querySelector("input[name=\"answer\"]");
    ui_form.addEventListener("submit", (e) => {
        e.preventDefault();

        checkQuestion(ui_form_input.value);
        ui_form_input.value = "";
        loadQuestion();

        return false;
    });

    var question;
    var loadQuestion = () => {
        question = matmat.getQuestion();
        questionText.text = question.question;
    }

    var checkQuestion = (answer) => {
        if(question.answer == answer) {
            runSpeed = 1;
        } else {
            runSpeed = 0;

            g.createParticles(
                runSprite.x, runSprite.y + 130, //x and y position
                () => g.sprite("img/dust.png"),   //Particle sprite
                g.stage,                       //The container to add the particles to  
                10,                            //Number of particles
                0,                             //Gravity
                false,                         //Random spacing
                0, Math.PI,                       //Min/max angle
                60 * runSpeed, 120 * runSpeed,                       //Min/max size
                5, 10                         //Min/max speed
                );
        }
    }

    var g = hexi(settings.width, settings.height, setup, settings.toLoad);
    g.start();

    scaleToWindow(ui);
    window.addEventListener("resize", function(event){ 
      scaleToWindow(ui);
    });

    g.scaleToWindow("white");
}

window.game.app();