#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json

data = json.loads('[{"level":1,"time":23545.90322580645},{"level":2,"time":5800.494505494506},{"level":3,"time":3993.5238095238096},{"level":4,"time":53635.765432098764},{"level":5,"time":3819.7567567567567},{"level":6,"time":104526.02857142857},{"level":7,"time":4053.4927536231885},{"level":8,"time":6103.308823529412},{"level":9,"time":22876.242424242424},{"level":10,"time":3706.753846153846},{"level":11,"time":44161.04651162791},{"level":12,"time":76371},{"level":13,"time":170455.25},{"level":14,"time":127030.72727272728},{"level":15,"time":13086.272727272728},{"level":16,"time":219972.0588235294},{"level":17,"time":44437.64705882353},{"level":18,"time":58558.42857142857},{"level":19,"time":121700.42857142857},{"level":20,"time":1339687.6666666667},{"level":21,"time":1804750},{"level":22,"time":177700.66666666666},{"level":26,"time":15072.5},{"level":23,"time":8993445},{"level":24,"time":71961},{"level":25,"time":50709},{"level":27,"time":135535}]')

timePerLevel = []
for entry in data:
	while len(timePerLevel) < entry['level']:
		timePerLevel.append(0)
        if entry['time'] > 3600000:
                continue; #filter longer than hour..
	timePerLevel[entry['level'] - 1] = entry['time']

import matplotlib.pyplot as plt

plt.plot(timePerLevel)
plt.savefig("level_time.png", bbox_inches="tight")
plt.show()
