var gulp = require('gulp');
var environments = require('gulp-environments');
var copy = require('gulp-contrib-copy');
var babel = require('gulp-babel');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var sourcemaps = require('gulp-sourcemaps');
var jade = require('gulp-jade');
var data = require('gulp-data');
var del = require('del');

var development = environments.development;
var production = environments.production;

gulp.task('clear', function() {
	del(['dist']);
});

gulp.task('libs', function() {
	gulp.src('lib/**/*')
	.pipe(copy())
	.pipe(gulp.dest('dist/'));

	gulp.src('node_modules/js-data/dist/*.min.*')
	.pipe(copy())
	.pipe(gulp.dest('dist/js'));

	gulp.src('node_modules/js-data-http/dist/*.min.*')
	.pipe(copy())
	.pipe(gulp.dest('dist/js'));
});

gulp.task('style', function() {
	return gulp.src('app/css/*')
	.pipe(copy())
	.pipe(gulp.dest('dist/css'));
});

gulp.task('img', function() {
	return gulp.src('app/img/*')
	.pipe(copy())
	.pipe(gulp.dest('dist/img'));
});

gulp.task('pack', function() {
	return gulp.src(['app/js/**/*.js'])
	.pipe(development(sourcemaps.init()))
	.pipe(babel({
		presets: ['es2015']
	}))
	.pipe(uglify())
	.pipe(concat('app.min.js'))
	.pipe(development(sourcemaps.write()))
	.pipe(gulp.dest('dist/js'));
});

gulp.task('templates', function() {
	return gulp.src('./app/**/*.jade')
	.pipe(data(function(file) {
		return require('./config/siteSettings.json');
	}))
	.pipe(jade({
		pretty: development()
	}))
	.pipe(gulp.dest('dist/'))
});

gulp.task('watch', function() {
	gulp.watch(['app/**/*.js'], ['pack']);
	gulp.watch(['lib/**/*'], ['libs']);
	gulp.watch(['app/css/*'], ['style']);
	gulp.watch(['app/*.jade', 'config/siteSettings.json'], ['templates']);
});

// The default task (called when you run `gulp` from cli)
gulp.task('default', ['libs', 'style', 'img', 'templates', 'pack', 'watch']);