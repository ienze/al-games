## isoDisplay
Visualization of progress using isometric plane splited like Voronoi diagram.

## How to run
Install and run gulp
```
$ npm install --global gulp-cli
$ gulp
```

Start any web server in dist/ directory.