import { Component } from 'react';
import io from 'socket.io-client';
import {Client} from '../../../diffsync';

class MultiplayerComponent extends Component {

	componentDidMount() {

		if(!this.multiplayerRoom) {
			throw new Error("MultiplayerComponent room name not specified!");
		}

		this.client = new Client(io(), this.multiplayerRoom);
	
		this.client.initialize();
	}

	sync() {
		this.client.sync();
	}

	componentWillUnmount() {
		this.client.close();
	}
}

export default MultiplayerComponent;