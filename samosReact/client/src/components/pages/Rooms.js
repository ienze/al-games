import React from 'react';
import MultiplayerComponent from '../MultiplayerComponent';
import {Link} from 'react-router';

class Rooms extends MultiplayerComponent {

/*
	constructor() {
		super("__rooms__");

		this.state = {
			r: 0
		};

		setInterval(() => {
			this.setState({
				r: this.state.r + 0.03
			});
		}, 100);
	}

	render() {
		return (
			<Stage>
				<Sprite image="http://ienze.me/static/media/me.bd4d9d7e.png" anchor={new PIXI.Point(0.5,0.5)} position={new PIXI.Point(400, 400)} rotation={this.state.r} />
			</Stage>
		);
	}
*/

	constructor() {
		super("__rooms__");
	}

	joinLobby(roomId) {

	}

	render() {
		return (
			<div id="page-rooms">
				<div className="rooms">
					<ul>
						{this.state.rooms.map(function(room, i){
					        return <li key={i}><Link to={"lobby/"+room.id}>{room.name}</Link></li>
					    })}
				    </ul>
				</div>
				<div className="createRoom">
					<form id="formCreateRoom">
						<input type="text" name="name" placeholder="Meno miestnosti" />
						<input type="submit" value="Vytvoriť miestnosť" />
					</form>
				</div>
			</div>
		);
	}

}

export default Rooms;