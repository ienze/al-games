import React, { Component } from 'react';
import MultiplayerComponent from '../MultiplayerComponent';
import {Stage, Sprite, Container, Text} from 'react-pixi';
import PIXI from 'pixi.js';
import gameConfig from '../../config/game.json';

class Lobby extends MultiplayerComponent {
	
	constructor() {
		super();

		this.playerTextures = self.g.filmstrip("img/players.png", 32, 32);

		this.multiplayerRoom = this.props.params.id;
		this.multiplayerInit = () => {
			if(!this.room) {
				this.room = {};
				this.room.players = [];
			}
			this.sync();
		}
	}

	render() {
		var width = window.innerWidth;
		var height = window.innerHeight;

		var textStyle = new PIXI.TextStyle({
			font : '21px Helvetica',
			fill : 0x000000
		});

		return (
			<Stage width={width} height={height}>
				<Container>
					<Text text={"Hraci: "+this.state.players.length+"/"+gameConfig.maxPlayers} x={width/2} y={8} anchor={new PIXI.Point(0.5, 0)} style={textStyle} />
					{this.state.countdown ?
						<Text text={this.state.countdown} x={width/2} y={height-8} anchor={new PIXI.Point(0.5, 1)} style={textStyle} />
					: null}
				</Container>
				<Sprite image="http://ienze.me/static/media/me.bd4d9d7e.png" anchor={new PIXI.Point(0.5,0.5)} position={new PIXI.Point(400, 400)} rotation={this.state.r} />
			</Stage>
		);
	}
}

export default Lobby;
