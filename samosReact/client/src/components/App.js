import React, { Component } from 'react';
import { Router, Route, IndexRoute, hashHistory } from 'react-router'

import Layout from './pages/Layout';
import Rooms from './pages/Rooms';
import Lobby from './pages/Lobby';
import Play from './pages/Play';
import Scoreboard from './pages/Scoreboard';
import NotFound from './pages/NotFound';

class App extends Component {
  render() {
    return (
      <Router history={hashHistory}>
        <Route path="/" component={Layout} >
          <IndexRoute component={Rooms} />
          <Route path="lobby/:id" component={Lobby} />
          <Route path="play/:id" component={Play} />
          <Route path="scoreboard/:id" component={Scoreboard} />
          <Route path="*" component={NotFound} />
        </Route>
      </Router>
    );
  }
}

export default App;
