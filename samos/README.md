## mqMove
Multiplayer game prototype using questions from learning system to move.

## How to run
Start server to handle multiplayer communication / webserver with game
```
$ npm run server
```

Start development enviroment
```
$ npm start
```